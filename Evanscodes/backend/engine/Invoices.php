<?php

/**
 * Created by PhpStorm.
 * User: marvin
 * Date: 1/10/17
 * Time: 10:02 AM
 */
class Invoices
{
    public function show($value)
    {
        $database = new Dbase();
        //this statement can be used to get both customers and invoice but i will stick to the requirement
//        $sql_statement = "SELECT i.*,c.name,c.id as customerId FROM invoices AS i JOIN customers AS c ON i.customers_id = c.id";
        $sql_statement = "SELECT * FROM invoices ";

        $invoices = $database->run($sql_statement);
        if ($value == 0)
        {
            return  $database->fetch($invoices);
        }

        $single_invoice = $database->run($sql_statement. " WHERE id = ".$value);

        return $database->fetch($single_invoice);
    }
}