<?php
class Client {
    
    public $customers;
    public $products;
    public $invoices;

    /**
     * Initializes the client facing section of the web store
     * 
     * @param Customers $customers
     * @param Products $products
     * @param Invoices $invoices
     */
    public function __construct(Customers $customers, Products $products, Invoices $invoices){

        $this->customers = $customers;
        $this->products = $products;
        $this->invoices = $invoices;
    }
    
    /**
     * Displays all the various aspects
     */
    public function showAll() {
        $this->invoices->show(0);
        //display the homepage table
        echo '<table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Invoice #</th>
                            <th>Customer</th>
                            <th>Total</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>';

        //loop invoices

        foreach ($this->invoices->show() as $key => $value) {
            setlocale(LC_MONETARY,"en_US.UTF-8");
            echo '<tr>
                        <td>0000' . $value['id'] . '</td>
                        <td>' . ucwords($this->customers->show($value['customers_id'])[0]['name']) . '</td>
                        <td> Ksh '.money_format('%!i',$value['total']).'</td>
                        <td><span class="label label-danger">' . $value['status'] . '</span></td>
                        <td><a href="?invoice=' . $value['id'] . '" class="btn btn-default" role="button">Show more</a></td>
                    </tr>';
        }
    }
    
    /**
     * Navigates to various sections based on input
     * 
     * @param type $io_type
     * @param type $io_value
     */
    public function show($io_type, $io_value) {
        
        switch ($io_type) {
            
            case 'invoice':
                //single invoice details
                $customer_details = $this->customers->show($this->invoices->show($io_value)[0]['customers_id'])[0];

                $products_details = json_decode($this->invoices->show($io_value)[0]['products']);

                    echo '<button class="btn btn-default pull-right" onclick="window.history.go(-1)">
                            Back to Listing</button><div class="well well-sm"><h4><strong>Web Store Invoice No. 0000'.$this->invoices->show($io_value)[0]['id'].'</strong>
                            </h4>
                            </div>
                            
                            <div class="row">                            
                            <div class="col-xs-12">
                                <div class="pull-left">
                                    <address>
                                      <strong>'.ucwords($customer_details['name']).'</strong><br>
                                      <abbr title="Mobile Phone">Phone:</abbr>'.$customer_details['telephone'].'<br/>
                                      <abbr title="Email Address">Email:</abbr> '. $customer_details['emailaddress'].'
                                      </address>
                                </div>
                                
                            <div c All accounts are to be paid within 7 days from receipt of
                                        invoice. To be paid by cheque or credit card or direct payment
                                        online. If account is not paid within 7 days the credits details
                                        supplied as confirmation of work undertaken will be charged the
                                        agrelass="pull-right">
                                <p class="m-t-10"><strong>Order Status: </strong> 
                                <span class="label label-danger">'.$this->invoices->show($io_value)[0]['status'].'</span></p>
                                <p class="m-t-10"><strong>Order ID: </strong> #0000'.$this->invoices->show($io_value)[0]['id'].'</p>
                            </div>
                            
                         </div><!-- end col -->
                        </div>
                        
                         <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                 All accounts are to be paid within 7 days from receipt of
                                        invoice. To be paid by cheque or credit card or direct payment
                                        online. If account is not paid within 7 days the credits details
                                        supplied as confirmation of work undertaken will be charged the
                                        agre
                                    <table class="table m-t-30">
                                         <thead class="bg-faded">
                                            <tr>
                                                <th>#</th>
                                                <th>Product No</th>
                                                <th>Item</th>
                                                <th>Description</th>
                                                <th>Quantity</th>
                                                <th>Total</th>
                                            </tr>
                                         </thead>
                                         <tbody>';

                                             $total_amount = 0;
                                             setlocale(LC_MONETARY,"en_US.UTF-8");
                                            foreach ($products_details as $key => $product_value) {

                                                $i = 1 + $key;

                                                $single_product_details = $this->products->show(json_decode($product_value)[0])[0];

                                                echo '<tr >
                                                    <td > '. $i.'</td >
                                                    <td > 0000'.$single_product_details['id'].'</td >
                                                    <td > '.$single_product_details['name'].'</td >
                                                    <td > '.$single_product_details['description'].'</td >
                                                    <td > 1</td >
                                                    <td > '.ucwords($single_product_details['code']). ' '.$single_product_details['price'].'</td >
                                                </tr >';

                                                $total_amount = $total_amount + $single_product_details['price'];

                                            }
                                     echo '</tbody>
                                            </table>
                                  </div>
                                </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="clearfix m-t-30">
                                    <h5 class="small text-inverse font-600"><b>
                                    PAYMENT TERMS AND POLICIES</b>
                                    </h5>
                                        <small>
                                            All accounts are to be paid within 7 days from receipt of
                                            invoice. To be paid by cheque or credit card or direct payment
                                            online. If account is not paid within 7 days the credits details
                                            supplied as confirmation of work undertaken will be charged the
                                            agreed quoted fee noted above.
                                    </small>
                                </div>
                            </div>
                            
                            <div class="col-md-3 col-sm-6 col-xs-6 col-md-offset-3">
                                <p class="text-xs-right"><b>Sub-total:</b> '. money_format('%!i',$total_amount) . '</p>
                                <p class="text-xs-right">Discout: 0%</p>
                                <p class="text-xs-right">VAT: 0%</p>
                                <hr>
                                <h3 class="text-xs-right">KSH '.money_format('%!i',$total_amount).'</h3>
                            </div>
                        </div>    
                    </div>';
                break;
        }
    }
}
